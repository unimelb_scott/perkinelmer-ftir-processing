from DuneClient import DuneClient
from SpectrumCSVReader import SpectrumCSVReader
from pathlib import Path
from uuid import uuid4
import pymongo
from sys import argv
from hashlib import sha1

if len(argv) != 2:
    print("wrong number of args bb")
    exit(-1)

dune_client = DuneClient()

file = SpectrumCSVReader(csv_file= Path(argv[1]))

#Case for 3 headers
if file.header_count == 3:
    wavenumbers = file.get_wavenumbers()
    absorbances = file.get_absorbances()


    hash_absorbances = sha1(str(absorbances).encode("utf-8")).hexdigest()
    hash_match = dune_client.coll.count_documents({"sha1":hash_absorbances})

    if file.headers["User"] and file.headers["LabID"] and hash_match==0:
        mongo_spectrum = {
            "_id" : str(uuid4()),
            "sha1": hash_absorbances,
            "user": file.headers["User"],
            "sampleName": file.headers["LabID"],
            "absorbances": file.get_absorbances(),
            "wavenumberInfo": {
                "start": wavenumbers[-1],
                "stop" : wavenumbers[0],
                "step" : (wavenumbers[0] - wavenumbers[1])
            }
        }
        dune_client.coll.insert_one(mongo_spectrum)
        print("Uploaded {}".format(file))
    elif dune_client.coll.count_documents({"sha1":hash_absorbances})!=0:
        print("SHA-1 matches existing file, check it has not been previously uploaded")
    else:
        print("Document does not contain User and/or LabID in file headers")
#Case for 2 headers
elif file.header_count == 2:
    wavenumbers = file.get_wavenumbers()
    absorbances = file.get_absorbances()


    hash_absorbances = sha1(str(absorbances).encode("utf-8")).hexdigest()
    hash_match = dune_client.coll.count_documents({"sha1":hash_absorbances})

    if file.headers["User"] and hash_match==0:
        mongo_spectrum = {
            "_id" : str(uuid4()),
            "sha1": hash_absorbances,
            "user": file.headers["User"],
            "sampleName": file.headers["LabID"],
            "absorbances": file.get_absorbances(),
            "wavenumberInfo": {
                "start": wavenumbers[-1],
                "stop" : wavenumbers[0],
                "step" : (wavenumbers[0] - wavenumbers[1])
            }
        }
        dune_client.coll.insert_one(mongo_spectrum)
        print("Uploaded {}".format(file))
    elif dune_client.coll.count_documents({"sha1":hash_absorbances})!=0:
        print("SHA-1 matches existing file, check it has not been previously uploaded")
    else:
        print("Document does not contain User and/or LabID in file headers")
else:
    print("This file structure is currently unsupported. Please contact Scott Essam for assistance")